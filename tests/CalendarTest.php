<?php

class CalendarTest extends PHPUnit_Framework_TestCase
{
    public function testConstruct()
    {
        $calendar = new \Calendar\Calendar();
        $this->assertInstanceOf('\Calendar\Calendar', $calendar);
    }

    public function testGetFirstDayOfWeek()
    {
        $calendar = new \Calendar\Calendar(null, \Calendar\Day::MONDAY);

        $this->assertEquals(\Calendar\Day::MONDAY, $calendar->getFirstDayOfWeek());
    }
    
    public function testGetMonthsNumber()
    {
        $calendar = new \Calendar\Calendar(12);
        
        $this->assertEquals(12, $calendar->getMonthsNumber());
    }

    public function testGetMonths()
    {
        $calendar = new \Calendar\Calendar(3);
        $calendar->calculateMonths();
        
        $this->assertEquals(3, sizeof($calendar->getMonths()));
    }

    public function testGetMonths2()
    {
        $calendar = new \Calendar\Calendar(3, \Calendar\Day::MONDAY, \Calendar\Month::MARCH);
        $calendar->calculateMonths();

        $lastMonth = array_pop($calendar->getMonths());

        $this->assertEquals(\Calendar\Month::MAY, $lastMonth->getNumber());
    }

    public function testCalculateMonths3()
    {
        $calendar = new \Calendar\Calendar(18, \Calendar\Day::TUESDAY, \Calendar\Month::JANUARY, 2016);
        $calendar->calculateMonths();

        $lastMonth = array_pop($calendar->getMonths());

        $this->assertEquals(\Calendar\Month::JUNE, $lastMonth->getNumber());
        $this->assertEquals(2017, $lastMonth->getYear());
    }

    public function testCalculateMonths4()
    {
        $calendar = new \Calendar\Calendar(12, \Calendar\Day::TUESDAY, \Calendar\Month::JANUARY, 2014);
        $calendar->calculateMonths();

        foreach ($calendar->getMonths() as $i => $month) {
            $this->assertEquals(0, sizeof($month->getDays()) % 7);
        }
    }

    public function testCalculateMonths5()
    {
        $calendar = new \Calendar\Calendar(12, \Calendar\Day::SUNDAY, \Calendar\Month::JANUARY, 2014);
        $calendar->calculateMonths();

        foreach ($calendar->getMonths() as $i => $month) {
            $this->assertEquals(0, sizeof($month->getDays()) % 7);
        }
    }

    public function testSetEvents()
    {
        $calendar = new \Calendar\Calendar(1, \Calendar\Day::SUNDAY, \Calendar\Month::FEBRUARY, 2014);
        $events = array(
            0 => array('date' => '2014-02-15', 'name' => 'test1'),
            1 => array('date' => '2014-02-18', 'name' => 'test2'),
            2 => array('date' => '2014-02-20', 'name' => 'test3'),
            3 => array('date' => '2014-02-21', 'name' => 'test4'),
        );
        $calendar->setEvents($events);
        $calendar->calculateMonths();

        $months = $calendar->getMonths();
        $days = $months[0]->getDays();
        $events = $days[25]->getEvents(); //20/02/2014

        $this->assertEquals($events[0]['name'], 'test3');
    }
}