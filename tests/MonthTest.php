<?php

class MonthTest extends PHPUnit_Framework_TestCase
{
    public function testConstruct()
    {
        $month = new \Calendar\Month();
        $this->assertInstanceOf('\Calendar\Month', $month);
    }

    public function testGetNumber()
    {
        $month = new \Calendar\Month(\Calendar\Month::MARCH, 2014);

        $this->assertEquals(\Calendar\Month::MARCH, $month->getNumber());
    }

    public function testGetNumber2()
    {
        $month = new \Calendar\Month(15, 2014);

        $this->assertEquals(\Calendar\Month::MARCH, $month->getNumber());
        $this->assertEquals(2015, $month->getYear());
    }

    public function testGetNumber3()
    {
        $month = new \Calendar\Month(13, 2014);

        $this->assertEquals(\Calendar\Month::JANUARY, $month->getNumber());
        $this->assertEquals(2015, $month->getYear());
    }

    public function testGetDaysNumber()
    {
        $month = new \Calendar\Month(\Calendar\Month::JANUARY, 2014);

        $this->assertEquals(31, $month->getDaysNumber());
    }

    public function testGetFirstDayOfMonthNumber()
    {
        $month = new \Calendar\Month(\Calendar\Month::JANUARY, 2014);

        $this->assertEquals(3, $month->getFirstDayNumber());
    }

    public function testGetDays()
    {
        $month = new \Calendar\Month(\Calendar\Month::JANUARY, 2014, \Calendar\Day::MONDAY);
          
        $this->assertEquals(35, sizeof($month->getDays()));
    }

}
