<?php

class DayTest extends PHPUnit_Framework_TestCase
{
    public function testConstruct()
    {
        $day = new \Calendar\Day();
        $this->assertInstanceOf('\Calendar\Day', $day);
    }

    public function testConstruct2()
    {
        $day = new \Calendar\Day(15, 1, 2014);
        
        $this->assertEquals(mktime(0, 0, 0, 1, 15, 2014), $day->getTimestamp());
    }

    public function testGetters()
    {
        $day = new \Calendar\Day(15, 1, 2014);
        $day->setDisabled(true);
        
        $this->assertEquals(15, $day->getNumber());
        $this->assertEquals(true, $day->isDisabled());
    }
}